#!/bin/sh
# goget.sh a simple shellscript to automagically download
# the libraries needed to compile GtkHtml2.
#
#  Written by: Thomas Nyberg <thomas@codefactory.se>
# Modified by: Jonas Borgstr�m <jonas@codefactory.se>

#args: checkout no-update

#You should set this to where you want the files to be installed.
#NOTE: these libraries are bleeding-edge, and should not be installed
#into /usr/ or /usr/local.

###########################################################################
# User settings
###########################################################################

MAKE=make                           # *BSD might need "gmake" here
base=/changeme
cvsdir=$base/gnomecvs               # Where the source is stored
prefix=$base/gtkhtml2-nightly       # Where the binaries are stored

###########################################################################
# No user editable parts below this
###########################################################################

mkdir -p $cvsdir
mkdir -p $prefix

if test "x$base" = "x/changeme" ; then
  echo "You should edit this file and change the base-variable to"
  echo "point to where the source should be compiled."
  exit 1 ;
fi

#environment-vars, do not touch!
export CVSROOT=':pserver:anonymous@anoncvs.gnome.org:/cvs/gnome'
export PATH=$prefix/bin:$PATH
export LD_LIBRARY_PATH=$prefix/lib:$LD_LIBRARY_PATH

case "x$1" in
  xcheckout) 
    always_checkout=yes 
    ;;
  xno-update) 
    never_update=yes 
    ;;
  x) 
    ;;
  *)
    echo "Options:"
    echo "checkout - do a checkout of every module, disregarding their already present"
    echo "no-update - do not do a cvs update on modules already present"  

    exit
    ;;
esac

#to prevent errors
rootpath=`pwd` 

cd $cvsdir
#download pkg-config 
if test -d pkgconfig -a "x$always_checkout" = "x" ; then
  if test "x$never_update" = "x" ; then
    cd pkgconfig
    cvs -z3 -d:pserver:anonymous@cvs.pkgconfig.sourceforge.net:/cvsroot/pkgconfig update -Pd || exit
    cd ..
  fi
else
  rm -rf pkgconfig
  if test "x`sed -n -e "/anonymous@cvs.pkgconfig.sourceforge.net/p" < $HOME/.cvspass`" = "x" ; then
      cvs -d:pserver:anonymous@cvs.pkgconfig.sourceforge.net:/cvsroot/pkgconfig login || exit
  fi
  cvs -z3 -d:pserver:anonymous@cvs.pkgconfig.sourceforge.net:/cvsroot/pkgconfig co pkgconfig || exit
fi  

( cd pkgconfig && rm -f config.cache && ./autogen.sh --prefix=$prefix && make install ) || \
( echo "Error occured while compinling pkgconfig. Without this, gtkhtml2 wont work. aborting..." && exit )

#modules we need:
# glib, pango, gtk+, gnome-xml, gnome-http
my_modules="glib pango gtk+ gnome-xml gnome-http"

#download all modules
for m in $my_modules gtkhtml2 ; do
  if test -d $m -a "x$always_checkout" = "x" ; then
    if test "x$never_update" = "x" ; then
      cd $m && cvs -z3 update -Pd || exit
      cd ..
    fi
  else
    rm -rf $m
    if test "x`sed -n -e "/anonymous@anoncvs.gnome.org/p" < $HOME/.cvspass`" = "x" ; then
	cvs login || exit
    fi
    cvs -z3 co $m || exit
  fi
done

#all done, make and install them
for m in $my_modules ; do
  if test -d $m ; then
    cd $m 
    ( rm -f config.cache && ./autogen.sh --prefix=$prefix --disable-static && $MAKE && $MAKE install ) || \
    ( echo "Error occured while compiling $m. Without this, gtkhtml2 wont work. aborting..." && exit )
    cd ..
  else
    echo "It seems that $m wasn't downloaded. aborting..."
    exit ;
  fi
done

#finally, compile it
cd gtkhtml2
( ./autogen.sh --prefix=$prefix --disable-static && $MAKE ) || echo "Error compiling gtkhtml2" 
cd $rootpath
