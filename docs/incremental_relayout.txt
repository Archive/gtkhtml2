This is my proposal for the new linelayout code, this will make "incremental relayout" possible:

Each paragraph (<p>, <div>, <pre>...) will create a html_box_block object. This only thing 
this box has to do is to put it's children in different line boxes.

There will be at least two types of line boxes:

* html_box_block_linebox:
  In this linebox goes objects with the css property "display: block", 
  only one box per line.

* html_box_inline_linebox:
  This line box will contain objects with the css property "display: inline", this linebox will try to
  fit as many inline boxes on the same line as possible.

The line boxes are just a small structure, so it will only waste a few bytes per row.

One example:

<html>
  <body>
    <p>
    Foo bar
    </p>
    <p>
    yada yada yada yada
    </p>
  </body>
</html>

The box structure will be like this:

html_box_block (html)
  html_box_block_linebox 
    html_box_block (body)
      html_box_block_linebox 
        html_box_block (p)
          html_box_inline_linebox 
            html_box_text (inline) "Foo bar"
        html_box_block (p)
          html_box_inline_linebox 
            html_box_text (inline) "yada yada yada yada"


If we now use the DOM interface to change the text "Foo bar" to "incremental relayout".
the relayout function is called on the parent box "p". The text is so long that that it has to be 
wrapped on two lines. Then the relayout function will call the relayout function of its parent (body) 
and tell it that he has changed.
Then The block box will skip to the line box that contains the changed box and if it is a 
html_box_block_linebox then the new height of that line is calculated and the other lineboxes below 
that line will just be moved down and then the relayout function of the parentbox will be called, 
this procedure will be repeated until the document root node is reached.

The float problem:

When a new linebox is created, then the width of the linebox will be set to the maximum right margin.
This way you can easily check if a float box now intersects the block box or if it has been moved away.
Every time the block box is relayouted, a new width is calculated and compared with the old one, and if
it differs from the old one, then the linebox has to be relayouted.

This might be a good way of doing line layouts, what do you think?

/ jonas Borgstr�m <jonas@codefactory.se>

