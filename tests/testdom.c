/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
   Copyright (C) 2000 CodeFactory AB
   Copyright (C) 2000 Jonas Borgstr�m <jonas@codefactory.se>
   Copyright (C) 2000 Anders Carlsson <andersca@codefactory.se>
   
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.
   
   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   
   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include <gtk/gtk.h>
#include "dom-core.h"
#include <libxml/parser.h>
#include <libgdome/gdome.h>
#include <libgdome/gdome-xml-str.h>
#include <libgdome/gdome-util.h>

static void
cb_eventl_attributeChange (GdomeEventListener *self, GdomeEvent
			   *event, GdomeException *exc)
{
	GdomeException ex;
	GdomeDOMTimeStamp ts;

	//                                                                             	ts = gdome_evnt_timeStamp (event, &ex);
	
	//	g_print ("attribute change event, time: %lld\n", gdome_evnt_timeStamp (event, &ex));
}

static void
cb_change_attr (GtkWidget *widget, gpointer data)
{
	DomDocument *gdoc = (DomDocument *)data;
	DomException exc;
	DomNodeList *nl;
	DomEventListener *listener;
	DomNode *node;

	g_print ("Calling get documentelement\n");
	node = dom_Document__get_documentElement (gdoc, &exc);

	nl = dom_Element__getElementsByTagName (node, dom_string_const_mkref ("p"), &exc);

	g_print ("Length is: %d\n", dom_NodeList__get_length (nl, &exc));
		 
	listener = _dom_EventListener__mkref (cb_eventl_attributeChange, NULL);

	node = dom_NodeList__item (nl, 0, &exc);

	g_print ("%s\n", dom_Node__get_nodeName (node, &exc)->str);
	
	dom_EventTarget__addEventListener ((DomEventTarget *)dom_NodeList__item (nl, 1, &exc),
					   dom_string_const_mkref ("DOMAttrModified"), listener, FALSE, &exc);

	dom_Element__setAttribute ((DomElement *)dom_NodeList__item (nl, 1, &exc),
				   dom_string_const_mkref ("align"),
				   dom_string_const_mkref ("center"), &exc);
//
//	gdome_el_setAttribute ((GdomeElement *)gdome_nl_item (nl, 1, 
//							      &exc),
//			       gdome_xml_str_mkref ("align"),
//							      gdome_xml_str_mkref ("center"), &exc);
//
//	g_print ("Change attr: %lu!\n", gdome_nl_length (nl, &exc));
//	gdome_n_namespaceURI (gdome_nl_item (nl, 1, &exc), &exc);
	
}

gint
main (gint argc, gchar **argv)
{
	xmlDocPtr doc;
	DomDocument *gdoc;

	GtkWidget *window, *button, *vbox;

	gtk_init (&argc, &argv);

	/* Parse the DOM structure from the xml tree */
	doc = xmlParseFile ("tests/domtest.xhtml");
	gdoc = _dom_Document__mkref (doc);

	vbox = gtk_vbox_new (FALSE, 0);
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_container_add (GTK_CONTAINER (window), vbox);
	
	button = gtk_button_new_with_label ("change attribute");
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (cb_change_attr), gdoc);
	gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);

	gtk_widget_show_all (window);
	gtk_main ();

	return 0;
}
